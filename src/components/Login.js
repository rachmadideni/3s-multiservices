import React from 'react';
import { Layout,Row,Col } from 'antd';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import './Login.css';
const { Header, Footer, Sider, Content } = Layout;
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class LoginForm extends React.Component {

	componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

	handleSubmit = (e) =>{
		e.preventDefault();
		this.props.form.validateFields((err,values) => {
			if(!err){
				console.log(values);
			}
		});
	}

	render(){
		/*{hasFeedback validateStatus="success"}*/
		/*hasFeedback validateStatus="validating"*/
		const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

		// Only show error after a field is touched.
	    const userNameError = isFieldTouched('username') && getFieldError('username');
	    const passwordError = isFieldTouched('password') && getFieldError('password');

		return (
			<div className="login-form" style={{ background:'#DADADA' }}>
				<Layout>
					<Content style={{ paddingTop: '150px',background: '#fff' }}>					
						<Row>														
							<Col span = { 6 } offset = { 9 }>
								<Form onSubmit = { this.handleSubmit }>
									
									<FormItem validateStatus = { userNameError ? 'error' : '' } help = { userNameError || '' } >
										{
											getFieldDecorator('username',{
												rules:[{
													required:true,
													message:'anda belum memasukkan username'
												}],
											})(<Input prefix={ <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} /> } placeholder="Username" />)}
									</FormItem>
									
									<FormItem validateStatus = { passwordError ? 'error' : '' } help = { passwordError || '' } >
									{
										getFieldDecorator('password',{
											rules:[{
												required:true,
												message:'anda belum memasukkan password'
											}],
										})(<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="password" />)
									}
									</FormItem>
									
									<FormItem>
										<a className="login-form-forgot" href="" style={{ paddingRight:'40px' }}>Forgot password</a>
								          <Button type="primary" htmlType="submit" className="login-form-button" disabled={hasErrors(getFieldsError())}>Log in</Button>
									</FormItem>

								</Form>
							</Col>
						</Row>
					</Content>
				</Layout>

			</div>

		);
	}
}
const WrappedLoginForm = Form.create()(LoginForm);
export default WrappedLoginForm;