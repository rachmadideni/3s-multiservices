import React from 'react';
import { Layout,Row,Col } from 'antd';
import { Form, Icon, Input, Button } from 'antd'; // , Checkbox
const { Content } = Layout; // Header, Footer, Sider, 
const FormItem = Form.Item;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class UserRegistration extends React.Component{
	componentDidMount(){

	}

	render(){
		const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

		// Only show error after a field is touched.
	    const userNameError = isFieldTouched('username') && getFieldError('username');
	    const passwordError = isFieldTouched('password') && getFieldError('password');

		return (
			<div className="login-form" style = {{ background: '#DADADA' }} >
				<Layout>
					<Content style={{ paddingTop: 50,background: '#fff' }}>
						<Row align = "middle" justify = "center" >
							<Col span = { 6 } offset = { 9 } style = {{ marginBottom: 15 }} >
								<h1>Lorem ipsum.</h1>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt quas magnam perferendis, mollitia eveniet quia.
							</Col>
						</Row>
						<Row>														
							<Col span = { 6 } offset = { 9 }  >{/*style = {{ background:'#CC0' }}*/}
								<Form layout = "vertical" onSubmit = { this.handleSubmit }>
									
									<FormItem label = "Alamat email anda" >
										<Input prefix = { <Icon type = "mail" style = {{ color: 'rgba(0,0,0,0.25)' }} /> } placeholder = "Alamat email" />
									</FormItem>

									<FormItem label = "Nama Depan">
										<Input prefix = { <Icon type = "user" style = {{ color: 'rgba(0,0,0,0.25)' }} /> } placeholder = "Nama depan" />
									</FormItem>

									<FormItem label = "Nama Belakang">
										<Input prefix = { <Icon type = "user" style = {{ color: 'rgba(0,0,0,0.25)' }} /> } placeholder = "Nama belakang" />
									</FormItem>									
									
									<FormItem label = "password" validateStatus = { passwordError ? 'error' : '' } help = { passwordError || '' } >
									{
										getFieldDecorator('password',{
											rules:[{
												required:true,
												message:'anda belum memasukkan password'
											}],
										})(<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="password" />)
									}
									</FormItem>

									<FormItem label = "konfirmasi password" validateStatus = { passwordError ? 'error' : '' } help = { passwordError || '' } >
									{
										getFieldDecorator('password_confirmation',{
											rules:[{
												required:true,
												message:'anda belum memasukkan password'
											}],
										})(<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="confirm password" />)
									}
									</FormItem>
									
									<FormItem>
										<Row>
											<Col span = { 24 } > 										
								          		<Button type="primary" size="large" style = {{ width:'100%' }} htmlType="submit" className="login-form-button" disabled={hasErrors(getFieldsError())}>Daftar</Button>
								          	</Col>
								        </Row>
									</FormItem>
									<FormItem>
										<Row>
											<Col style = {{ textAlign:'center' }} >
												<span>already have an account ?</span>											
											</Col>
										</Row>
										<Row>
											<Col span = { 12 } style = {{ textAlign : 'center' }} >
												<a className="login-form-forgot" href="">Login</a>
											</Col>
											<Col span = { 12 } style = {{ textAlign : 'center' }} >
												<a className="login-form-forgot" href="">Forgot password</a>
											</Col>
										</Row>										
									</FormItem>

								</Form>
							</Col>
						</Row>
					</Content>
				</Layout>
			</div>
		);
	}
}

const WrappedUserRegistration = Form.create()(UserRegistration);
export default WrappedUserRegistration;