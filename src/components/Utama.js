import React from 'react';
import { Layout, Menu, Icon, Breadcrumb, Row, Col } from 'antd';
// import './Utama.css';

const { Header, Sider, Content } = Layout; // Header, Footer, Sider,
const SubMenu = Menu.SubMenu;

class Utama extends React.Component {
	render(){
		return (
			<Layout>
				<Sider trigger = { null } collapsible collapsed = { true } width = { 300 } style = {{ background:'rgb(237,20,111)',left:0,overflow: 'auto', height: '100vh' }} >					
					<Menu theme="dark" mode="inline" style={{ height: '100%', borderRight: 0, paddingTop:25,paddingRight:25,paddingBottom:0, background:'rgb(237,20,111)' }} >
						<Menu.Item key="1" style = {{ paddingLeft:0,marginLeft:0 }} >
							<Icon type="home" style = {{ fontSize:22,color:'#fff' }} />
							<span>Home</span>
						</Menu.Item>
						<Menu.Item key="2">
							<Icon type="mail" style = {{ fontSize:22,color:'#fff' }} />
							<span>Mail</span>
						</Menu.Item>
						<Menu.Item key="3">
							<Icon type="cloud" style = {{ fontSize:22,color:'#fff' }} />
							<span>billing</span>
						</Menu.Item>
						<Menu.Item key="3">
							<Icon type="poweroff" style = {{ fontSize:22,color:'#fff' }} />
							<span>logout</span>
						</Menu.Item>						
					</Menu>
				</Sider>
				<Sider trigger = { null } collapsible collapsed = { false } width = { 250 } style = {{ background:'#F4F5F7' }}>
					<div className="logo" style = {{ height: '32px', background: '#F4F5F7',margin: '16px' }}>
						<h3>NAMA DEALER</h3>
					</div>
					<Menu theme="light" mode="inline" style={{ height: '100%', borderRight: 0, background:'#F4F5F7' }} >
						<Menu.Item key="1">
							<Icon type="dashboard" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Dashboard</span>
						</Menu.Item>

						<Menu.Item key="2">
							<Icon type="table" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Stock</span>
						</Menu.Item>

						<Menu.Item key="3">
							<Icon type="select" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Sales</span>
						</Menu.Item>

						<Menu.Item key="4">
							<Icon type="form" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Services</span>
						</Menu.Item>

						<Menu.Item key="5">
							<Icon type="user" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Parts</span>
						</Menu.Item>
						
						<Menu.Item key="6">
							<Icon type="setting" style = {{ fontSize:18,fontWeight:'bold',color:'black' }} />
							<span>Settings</span>
						</Menu.Item>
						<SubMenu key="sub1" title={<span><Icon type="user" /><span>User</span></span>} >
							<Menu.Item key="3">Tom</Menu.Item>
							<Menu.Item key="4">Bill</Menu.Item>
							<Menu.Item key="5">Alex</Menu.Item>
						</SubMenu>
					</Menu>
				</Sider>
				<Layout>
					
					<Content style={{ padding: 24, background: '#fff', minHeight: 780 }}>
						<Breadcrumb>							
							<Breadcrumb.Item href="">								
								<span>deni rachmadi</span>
							</Breadcrumb.Item>							
						</Breadcrumb>
						
							<Content style = {{ marginTop:15 }} >
								<Row>
									<h2>Dashboard</h2>
								</Row>
							</Content>
						
					</Content>
				</Layout>
			</Layout>
		);
	}
}

export default Utama;