import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import IndexPage from './routes/IndexPage';
import Products from './routes/Products';
import UserRegistration from './routes/UserRegistration';

// test login Component
//import UserRegistration from './components/UserRegistration';

import Login from './components/Login';
import Utama from './components/Utama';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={IndexPage} />
        <Route path="/products" exact component = { Products } />
        <Route path="/login" exact component = { Login } />
        <Route path="/registration" exact component = { UserRegistration } />
        <Route path="/utama" exact component = { Utama } />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
