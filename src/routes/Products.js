import React from 'react';
import { connect } from 'dva';
import ProductList from '../components/ProductList';

const Products = ({ dispatch, products }) => {

	function handleDelete(id){
		dispatch({
		  type: 'products/delete', // if in model outside, need to add namespace
		  payload: id
		});

	}

	return (
		<div>
			<h2>list of products</h2>
			<ProductList onDelete = { handleDelete } products = { products } />
		</div>		
	);
};

export default connect(({ products }) => ({ products,}))(Products);